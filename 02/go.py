#!/usr/bin/python3

from functools import reduce

fin = 'input'

def calculateChecksum(data):
	statistics = reduce(lambda prv, nxt: {'twos': prv['twos'] + nxt['twos'], 'threes': prv['threes'] + nxt['threes']}, data)
	# print(statistics)

	return statistics['twos'] * statistics['threes']

def generateSimilarityData(idData):
	id = idData['value']

	data = idData['allValues']

	similarities = list(filter(lambda x: x != id and x != '', list(map(lambda datum: ''.join([value for index, value in enumerate(datum) if id[index] == datum[index]]), data))))

	greatestSimilarities = reduce(lambda prv, nxt: prv if len(prv) > len(nxt) else nxt, similarities) if len(similarities) > 0 else ''

	return {'id': id, 'similarities': similarities, 'similarest': greatestSimilarities}

def getInput(fin):
	infile = open(fin, 'r')

	data = []
	while True:
		line = infile.readline()

		if line == '':
			break

		data.append(line.strip())

	infile.close()

	return data

def processId(id):
	id = list(id)

	twos = [c for c in id if id.count(c) == 2]
	threes = [c for c in id if id.count(c) == 3]

	# numTwos = len(twos) / 2
	# numThrees = len(threes) / 3

	numTwos = 0 if len(twos) == 0 else 1
	numThrees = 0 if len(threes) == 0 else 1

	return {'twos': numTwos, 'threes': numThrees, 'id': ''.join(id)}

data = getInput(fin)

processedTwosAndThrees = list(map(processId, data))
# print(processedTwosAndThrees)

checksum = calculateChecksum(processedTwosAndThrees)

print('Checksum:', checksum)

similarityData = list(map(generateSimilarityData, [{'value': elt, 'allValues': data} for elt in data]))

# print(similarityData)

mostSimilarIds = list(filter(lambda datum: len(datum['id']) == len(datum['similarest']) + 1, similarityData))

# print(mostSimilarIds)

commonLetters = mostSimilarIds[0]['similarest'] if len(mostSimilarIds) > 0 else 'ERROR'

print('Common letters in ID:', commonLetters)