#!/usr/bin/python3

import os
from functools import reduce

def getFrequencies(fin):
	history = []

	infile = open(fin, 'r')

	while True:
		line = infile.readline()

		if line == '':
			break

		line = line.strip()

		history.append(int(line))

	return history

totalUp = lambda prv, nxt: prv + nxt

fin = 'input'

freqList = getFrequencies('input')

sum = reduce(totalUp, freqList)

print('Overall frequency:', sum)

print('Determining first repeated frequency...')
print('Pass', 1, '...')

passes = 1
i = 0
sum = 0
history = []

while True:
	sum += freqList[i]

	if sum in history:
		print('First repeated frequency:', sum)

		break

	history.append(sum)

	i += 1

	if i == len(freqList):
		passes += 1
		print('Pass', passes, '...')
		i = 0
