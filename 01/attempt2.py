#!/usr/bin/python3

fin = 'input'

from functools import reduce
from itertools import accumulate

def computeRepeats(datum):
	value = datum['value']

	cumulativeSum = datum['cumulativeSum']

	total = cumulativeSum[len(cumulativeSum) - 1]

	numberOfCycles = list(map(lambda x: {'cycles': (x - value) / total, 'value': x}, cumulativeSum))

	numberOfCycles = list(filter(lambda x: x['cycles'] > 0 and int(x['cycles']) == x['cycles'], numberOfCycles))

	numberOfCycles = reduce(lambda prv, nxt: prv if prv['cycles'] <= nxt['cycles'] else nxt, numberOfCycles) if len(numberOfCycles) > 0 else []

	return {'value': value, 'cycles': numberOfCycles}

def readData(fin):
	data = []

	infile = open(fin, 'r')
	while True:
		line = infile.readline()

		if line == '':
			break

		data.append(int(line.strip()))

	return data

data = readData(fin)

cumulativeSum = list(accumulate(data))

overallFrequency = cumulativeSum[len(cumulativeSum) - 1]

print('Overall Frequency:', overallFrequency)

data = list(map(lambda datum: {'value': datum, 'cumulativeSum': cumulativeSum}, cumulativeSum))

# print(data)

data = list(map(computeRepeats, data))

# print(data)

validRepeats = list(filter(lambda x: len(x['cycles']) > 0, data))

# print(validRepeats)

leastRequiredCycles = reduce(lambda prev, next: prev if prev['cycles']['cycles'] <= next['cycles']['cycles'] else next, validRepeats)

# print(leastRequiredCycles)

firstRepeatedFrequency = leastRequiredCycles['cycles']['value']

print('First repeated frequency:', firstRepeatedFrequency)